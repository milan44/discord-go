## Usage

Import the package
```golang
import "gitlab.com/milan44/discord-go"
```

### Webhooks

```golang
r := discord.RichEmbed{
    Title:       "Hello World",
    Description: "This is a cool embed",
}
r.HexColor("#ff9933")

m := discord.Message{}
m.AddEmbed(r)

w := discord.Webhook{
    URL:  "webhook url",
    Name: "My cool webhook",
}
err := w.Execute(m)
if err != nil {
    panic(err)
}
```

![webhook](images/webhook.png)

### Bots

```golang
r := discord.RichEmbed{
    Title:       "Hello World",
    Description: "This is a cool embed",
}
r.HexColor("#ff9933")

m := discord.NormalMessage{
    Embed: r,
    TTS:   false,
}

bot := discord.Bot{
    Token: "bot token",
}

err := bot.SendMessage("channelID", m)
if err != nil {
    panic(err)
}
```

![bot](images/bot.png)