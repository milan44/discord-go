package discord

// Webhook defines a discord webhook
type Webhook struct {
	URL  string
	Name string
	Icon string
}

// Execute sends the embed via a webhook
func (w Webhook) Execute(m Message) error {
	data := map[string]interface{}{}
	if m.Content != "" {
		data["content"] = m.Content
	}
	if len(m.Embeds) > 0 {
		data["embeds"] = m.Embeds
	}
	if w.Name != "" {
		data["username"] = w.Name
	}
	if w.Icon != "" {
		data["avatar_url"] = w.Icon
	}

	req, err := createJSONPostRequest(w.URL+"?wait=true", data)
	if err != nil {
		return err
	}

	result, err := executeJSONRequest(req)
	if err != nil {
		return err
	}

	return checkDiscordMessageRespose(result)
}
