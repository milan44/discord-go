package discord

import (
	"strconv"
	"strings"
	"time"
)

// EmbedFooter defines a discord rich embed footer
type EmbedFooter struct {
	Text string `json:"text"`
	Icon string `json:"icon_url"`
}

// EmbedImage defines a discord rich embed image
type EmbedImage struct {
	URL string `json:"url"`
}

// EmbedAuthor defines a discord rich embed author
type EmbedAuthor struct {
	Name string `json:"name"`
	URL  string `json:"url"`
	Icon string `json:"icon_url"`
}

// EmbedField defines a discord rich embed field
type EmbedField struct {
	Name   string `json:"name"`
	Value  string `json:"value"`
	Inline bool   `json:"inline"`
}

// HexColor sets the color of the rich embed given a hex color
func (r *RichEmbed) HexColor(hex string) error {
	if strings.HasPrefix(hex, "#") {
		hex = strings.ReplaceAll(hex, "#", "")
	}

	n, err := strconv.ParseUint(hex, 16, 32)
	if err != nil {
		return err
	}

	if n < 0 {
		n = 0
	} else if n > 16777215 {
		n = 16777215
	}

	r.Color = int(n)
	return nil
}

// TimestampNow sets the timestamp to now
func (r *RichEmbed) TimestampNow() {
	r.SetTime(time.Now())
}

// SetTimestamp sets the timestamp
func (r *RichEmbed) SetTimestamp(timestamp int64) {
	r.SetTime(time.Unix(timestamp, 0))
}

// SetTime sets the timestamp based on a time
func (r *RichEmbed) SetTime(t time.Time) {
	r.Timestamp = t.Format(time.RFC3339)
}
