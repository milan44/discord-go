package discord

import "testing"

// TestMessage tests the discord message sending
func TestMessage(t *testing.T) {
	w := Webhook{
		URL:  "[webhook url removed]",
		Name: "Webhook Name",
		Icon: "https://dummyimage.com/300x300/000/fff.png&text=webhook",
	}
	err := w.Execute(createMessage())
	if err != nil {
		panic(err)
	}
}

func createMessage() Message {
	r := RichEmbed{
		Title:       "Title",
		Description: "Description",
		URL:         "https://dummyimage.com/300x300/000/fff.png&text=embed+url",
		Footer: EmbedFooter{
			Text: "Footer text",
			Icon: "https://dummyimage.com/300x300/000/fff.png&text=footer+icon",
		},
		Image: EmbedImage{
			URL: "https://dummyimage.com/300x300/000/fff.png&text=image",
		},
		Thumbnail: EmbedImage{
			URL: "https://dummyimage.com/300x300/000/fff.png&text=thumbnail",
		},
		Author: EmbedAuthor{
			Name: "Author",
			URL:  "https://dummyimage.com/300x300/000/fff.png&text=author+url",
			Icon: "https://dummyimage.com/300x300/000/fff.png&text=author+icon",
		},
	}

	r.TimestampNow()
	r.HexColor("#ff9933")

	r.AddField("Name (not inline)", "value", false)
	r.AddField("Name (inline)", "value", true)
	r.AddField("Name (inline)", "value", true)

	m := Message{
		Content: "Some content",
	}
	m.AddEmbed(r)

	return m
}

func TestReadMeWebhook(t *testing.T) {
	r := RichEmbed{
		Title:       "Hello World",
		Description: "This is a cool embed",
	}
	r.HexColor("#ff9933")

	m := Message{}
	m.AddEmbed(r)

	w := Webhook{
		URL:  "webhook url",
		Name: "My cool webhook",
	}
	err := w.Execute(m)
	if err != nil {
		panic(err)
	}
}

func TestReadMebot(t *testing.T) {
	r := RichEmbed{
		Title:       "Hello World",
		Description: "This is a cool embed",
	}
	r.HexColor("#ff9933")

	m := NormalMessage{
		Embed: r,
		TTS:   false,
	}

	bot := Bot{
		Token: "bot token",
	}

	err := bot.SendMessage("channelID", m)
	if err != nil {
		panic(err)
	}
}

func TestBot(t *testing.T) {
	m := createMessage()

	mess := NormalMessage{
		Content: m.Content,
		Embed:   m.Embeds[0],
		TTS:     false,
	}

	bot := Bot{
		Token: "bot token",
	}

	err := bot.SendMessage("channelID", mess)
	if err != nil {
		panic(err)
	}
}
