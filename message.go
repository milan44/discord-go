package discord

import (
	"encoding/json"
	"errors"
)

// Message defines a discord webhook message
type Message struct {
	Content string      `json:"content"`
	Embeds  []RichEmbed `json:"embeds"`
}

// AddEmbed adds an embed to the message
func (m *Message) AddEmbed(embed RichEmbed) error {
	if len(m.Embeds) >= 10 {
		return errors.New("A message can have a maximum of 10 embeds")
	}

	m.Embeds = append(m.Embeds, embed)
	return nil
}

// JSON converts the Message to Json
func (m Message) JSON() ([]byte, error) {
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, err
}
