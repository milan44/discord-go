package discord

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func createJSONPostRequest(url string, data interface{}) (*http.Request, error) {
	b, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

func executeJSONRequest(req *http.Request) (map[string]interface{}, error) {
	client := http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func checkDiscordMessageRespose(response map[string]interface{}) error {
	if response["id"] == nil {
		if response["message"] != nil {
			return errors.New("An error occured while sending message (Error: " + response["message"].(string) + ")")
		} else if response["_misc"] != nil {
			return errors.New("An error occured while sending message (Error: " + response["_misc"].(string) + ")")
		}
		return errors.New("An unknown error occured while sending message")
	}
	return nil
}
