package discord

import (
	"errors"
)

// RichEmbed defines a discord rich embed
type RichEmbed struct {
	Title       string       `json:"title"`
	Description string       `json:"description"`
	URL         string       `json:"url"`
	Timestamp   string       `json:"timestamp"`
	Color       int          `json:"color"`
	Footer      EmbedFooter  `json:"footer"`
	Image       EmbedImage   `json:"image"`
	Thumbnail   EmbedImage   `json:"thumbnail"`
	Author      EmbedAuthor  `json:"author"`
	Fields      []EmbedField `json:"fields"`
}

// AddField adds a field to the embed
func (r *RichEmbed) AddField(name, value string, inline bool) error {
	if name == "" || value == "" {
		return errors.New("Name or value cannot be empty")
	} else if len(name) > 256 || len(value) > 1024 {
		return errors.New("Name has to be max 256 characters and value has to be max 1024 characters")
	} else if len(r.Fields) >= 25 {
		return errors.New("An embed can have a maximum of 25 fields")
	}

	r.Fields = append(r.Fields, EmbedField{
		Name:   name,
		Value:  value,
		Inline: inline,
	})
	return nil
}
