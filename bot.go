package discord

// Bot defines a discord bot
type Bot struct {
	Token string
}

// NormalMessage defines a discord message that is able to be sent by a bot
type NormalMessage struct {
	Content string    `json:"content"`
	Embed   RichEmbed `json:"embed"`
	TTS     bool      `json:"tts"`
}

// SendMessage sends a message in a channel
func (b Bot) SendMessage(channelID string, message NormalMessage) error {
	req, err := createJSONPostRequest("https://discord.com/api/v6/channels/"+channelID+"/messages", message)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", "Bot "+b.Token)

	result, err := executeJSONRequest(req)
	if err != nil {
		return err
	}

	return checkDiscordMessageRespose(result)
}
